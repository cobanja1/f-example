# README #

This README specifies the manual on how to run this project

### Requirements ###
Laravel 8.x
NodeJS 14.x
PHP 8.x
Composer
MySQL 8.x

### How do I make it run? ###

* Create the database (Database installation guidelines below)
* $ cd path-to-repository-folder
* $ cp .env.example .env
*
* Add database connection details to .env file
*
* $ composer update
* $ php artisan key:generate
* $ npm install
* $ npm run dev
* $ php artisan serve

### Who do I talk to? ###

* the repo owner please or the person who sent you the link

### Database installation guidelines ###

CREATE SCHEMA `f_example`;
CREATE TABLE `f_example`.`purchases` (
  `purchase_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL DEFAULT (curdate()),
  `qty` SMALLINT UNSIGNED NOT NULL DEFAULT '0',
  `unit_price` DECIMAL(9,2) UNSIGNED NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`purchase_id`));
CREATE TABLE `f_example`.`applications` (
  `application_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL DEFAULT (curdate()),
  `qty` SMALLINT UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`application_id`));
INSERT INTO `f_example`.`purchases` (`date`, `qty`, `unit_price`) VALUES
  ('2020-06-05', 10, 5),
  ('2020-06-07', 30, 4.5),
  ('2020-06-09', 10, 5),
  ('2020-06-10', 34, 4.5),
  ('2020-07-10', 47, 4.3),
  ('2020-07-13', 10, 5),
  ('2020-07-25', 50, 4.2),
  ('2020-07-31', 10, 5),
  ('2020-08-14', 15, 5),
  ('2020-08-17', 3, 6),
  ('2020-08-29', 2, 7);
INSERT INTO `f_example`.`applications` (`date`, `qty`) VALUES
  ('2020-06-08', 20),
  ('2020-06-15', 25),
  ('2020-06-23', 37),
  ('2020-07-12', 38),
  ('2020-07-26', 28),
  ('2020-08-31', 30);