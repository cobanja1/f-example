<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\{
  Purchase,
  Application
};
use Illuminate\Http\{
  Request,
  Response
};
use \Illuminate\Contracts\Routing\ResponseFactory;

class InventoryController extends Controller
{
  /**
   * The current limit for this example app.
   *
   * @var int
   */
  public const LIMIT = 1000; // Synced to frontend

  /**
   * The quantity to check.
   *
   * @var int
   */
  private $qty;

  /**
   * The array of purchases used.
   *
   * @var array
   */
  private $purchases_to_process;

  /**
   * The quantity applied.
   *
   * @var int
   */
  private $total_applied;

  /**
   * Getting filled in case the validation is unsuccessful.
   *
   * @var string
   */
  public $error = '';

  /**
   * @param string $qty - needs to stay string for validating floats
   */
  public function isValid(string $qty): bool
  {
    if (empty($qty) || !is_numeric($qty)) {
      $this->error = 'The provided quantity is incorrect...';
      return false;
    }

    $qty_int = (int) $qty;
    $qty_float = (float) $qty;

    switch (true) {
      case $qty_int <= 0:
        $this->error = 'The provided quantity should be a positive number...';
        return false;

      case $qty_int != $qty_float:
        $this->error = 'The provided quantity cannot be decimal...';
        return false;

      case $qty_int > self::LIMIT:
        $this->error = 'The provided quantity should be between 0 and ' . self::LIMIT . '...';
        return false;

      default:
        return true;
    }
  }

  public function setQty(int $qty): void
  {
    $this->qty = $qty;
  }

  public function setPurchasesToProcess(array $arr): void
  {
    $this->purchases_to_process = $arr;
  }

  public function setAppliedQty(int $total_applied): void
  {
    $this->total_applied = $total_applied;
  }

  public function getMoneyValue(int $qty): float
  {
    $key_accum = Purchase::KEY_ACCUM;

    if (is_null($this->total_applied))
      $this->setAppliedQty(Application::getAppliedQty());

    $total_to_apply = $this->total_applied + $qty;

    if (empty($this->purchases_to_process))
      $this->setPurchasesToProcess(Purchase::getPurchasesToProcess((int) $this->total_applied, $total_to_apply));

    $money_value = 0.00;
    $purchases_count = count($this->purchases_to_process);
    $last_purchase_index = $purchases_count - 1;

    // Checking if all has been applied or there is enough items to make the application
    if (!$purchases_count || $this->purchases_to_process[$last_purchase_index]->{$key_accum} < $total_to_apply)
      return $money_value;

    // Trimming the used values from the first item
    $this->purchases_to_process[0]->{Purchase::KEY_QTY} = $this->purchases_to_process[0]->{$key_accum} - $this->total_applied;

    // Trimming the qty to be applied from the last item
    $this->purchases_to_process[$last_purchase_index]->{Purchase::KEY_QTY} -= array_sum(array_column($this->purchases_to_process, Purchase::KEY_QTY)) - $qty;

    foreach ($this->purchases_to_process as $p)
      $money_value += $p->{Purchase::KEY_QTY} * $p->{Purchase::KEY_UNIT_PRICE};

    return $money_value;
  }

  public function checkOnHand(Request $request): Response | ResponseFactory
  {
    if (!$this->isValid((string) ($request->input('qty') ?? '')))
      return response(json_encode(['status' => false, 'text' => $this->error]), self::GENERIC_STATUS);

    $this->setQty((int) $request->input('qty'));
    $value = $this->getMoneyValue($this->qty);

    if ($value <= 0)
      return response(json_encode(['status' => false, 'text' => 'Could not calculate the value...']), self::GENERIC_STATUS);

    return response(json_encode(['status' => true, 'text' => $value]), self::GENERIC_STATUS);
  }
}
