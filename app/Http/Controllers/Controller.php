<?php

namespace App\Http\Controllers;

use \Illuminate\Foundation\{
  Auth\Access\AuthorizesRequests,
  Bus\DispatchesJobs,
  Validation\ValidatesRequests
};
use \Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  /**
   * Keeping this variable separated, so it can go to the global scope if needed by other functionalities
   *
   * @var string
   */
  protected const UNKNOWN_ERR = 'Unknown error';

  /**
   * This controller returns 200 for every response. 40X, 50X ones are supposed to be outside of it's scope
   *
   * @var int
   */
  protected const GENERIC_STATUS = 200;
}
