<?php

declare(strict_types=1);

namespace App\Models;

use \Illuminate\{
  Database\Eloquent\Model,
  Support\Facades\DB
};

class Application extends Model
{
  public const TABLE = 'f_example.applications';

  public const PR_KEY = 'application_id';
  public const KEY_DATE = 'date';
  public const KEY_QTY = 'qty';

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public $table = self::TABLE;

  /**
   * The primary key associated with the table.
   *
   * @var string
   */
  protected $primaryKey = self::PR_KEY;

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = false;

  public static function getAppliedQty(): int
  {
    return (int) DB::table(Application::TABLE)->sum(Application::KEY_QTY);
  }
}