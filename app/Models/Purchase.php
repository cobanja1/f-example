<?php

declare(strict_types=1);

namespace App\Models;

use \Illuminate\{
  Database\Eloquent\Model,
  Support\Facades\DB
};

class Purchase extends Model
{
  public const TABLE = 'f_example.purchases';

  public const PR_KEY = 'purchase_id';
  public const KEY_DATE = 'date';
  public const KEY_QTY = 'qty';
  public const KEY_UNIT_PRICE = 'unit_price';

  /**
   * The table associated with the model.
   *
   * @var string
   */
  public $table = self::TABLE;

  /**
   * The primary key associated with the table.
   *
   * @var string
   */
  protected $primaryKey = self::PR_KEY;

  /**
   * The key for accumulation value.
   *
   * @var string
   */
  public const KEY_ACCUM = 'accum';

  /**
   * Indicates if the model should be timestamped.
   *
   * @var bool
   */
  public $timestamps = false;

  public static function getPurchasesToProcess(int $total_applied, int $total_to_apply): array
  {
    $tmp_table = 'acc_table';

    $accum = DB::table(self::TABLE)
      ->select(self::TABLE . '.*', DB::raw('(@acc := @acc + ' . Purchase::TABLE . '.' . self::KEY_QTY . ') as ' . self::KEY_ACCUM))
      ->crossJoin(DB::raw('(SELECT @acc := 0) p'));

    $purchases = DB::table($accum, $tmp_table)
      ->select($tmp_table . '.*')
      ->orderBy(self::KEY_DATE)
      ->orderBy(self::PR_KEY)
      ->where(self::KEY_ACCUM, '>', $total_applied)
      ->where(function ($q) use ($total_to_apply) {
        $q->where(self::KEY_ACCUM, '<', $total_to_apply)
          ->orWhere(function ($q) use ($total_to_apply) {
            $q->where(self::KEY_ACCUM, '>=', $total_to_apply)->where(DB::raw(self::KEY_ACCUM . '-' . Purchase::KEY_QTY), '<', $total_to_apply);
          });
      })->get();

    return (array) $purchases->getIterator();
  }
}