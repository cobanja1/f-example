<?php

namespace Tests\Unit\Controllers;

use \PHPUnit\Framework\TestCase;
use \App\Http\Controllers\InventoryController;
use \stdClass;
use \TypeError;

class InventoryControllerTest extends TestCase
{
  private static $applied_qty = 178;
  private static $purchases = [
    [
      'purchase_id' => 7,
      'date' => '2020-07-25',
      'qty' => 50,
      'unit_price' => 4.20,
      'accum' => 191
    ],
    [
      'purchase_id' => 8,
      'date' => '2020-07-31',
      'qty' => 10,
      'unit_price' => 5.00,
      'accum' => 201
    ],
    [
      'purchase_id' => 7,
      'date' => '2020-08-14',
      'qty' => 15,
      'unit_price' => 5.00,
      'accum' => 216
    ]
  ];

  private static function purchaseObjectsArray()
  {
    $arr = [];

    foreach (self::$purchases as $p) {
      $obj = new stdClass();

      foreach ($p as $k => $v)
        $obj->{$k} = $v;

      $arr[] = $obj;
    }

    return $arr;
  }

  public function testCanBeInstantiated(): void
  {
    $this->assertInstanceOf(
      InventoryController::class,
      new InventoryController()
    );
  }

  public function testInvalidSetQtyArgument(): void
  {
    $this->expectException(TypeError::class);
    $obj = new InventoryController();
    $obj->setQty('invalid');
  }

  public function testQtyEmpty(): void
  {
    $obj = new InventoryController();

    $this->assertEquals(
      false,
      $obj->isValid(''),
      $obj->error
    );
  }

  public function testQtyFloat(): void
  {
    $obj = new InventoryController();

    $this->assertEquals(
      false,
      $obj->isValid('2.44'),
      $obj->error
    );
  }

  public function testQtyNonNumeric(): void
  {
    $obj = new InventoryController();

    $this->assertEquals(
      false,
      $obj->isValid('some_text'),
      $obj->error
    );
  }

  public function testQtyIntString(): void
  {
    $obj = new InventoryController();

    $this->assertEquals(
      false,
      $obj->isValid('32some_text'),
      $obj->error
    );
  }

  public function testQtyNegative(): void
  {
    $obj = new InventoryController();

    $this->assertEquals(
      false,
      $obj->isValid('-3'),
      $obj->error
    );
  }

  public function testQtyOnLimit(): void
  {
    $obj = new InventoryController();

    $this->assertEquals(
      true,
      $obj->isValid((string) InventoryController::LIMIT),
      $obj->error
    );
  }

  public function testQtyHugeValue(): void
  {
    $obj = new InventoryController();

    $this->assertEquals(
      false,
      $obj->isValid((string) (InventoryController::LIMIT + 1)),
      $obj->error
    );
  }

  public function testApplicationOf35(): void
  {
    $obj = new InventoryController();
    $obj->setPurchasesToProcess(self::purchaseObjectsArray());
    $obj->setAppliedQty(self::$applied_qty);

    $this->assertEquals(
      164.6,
      $obj->getMoneyValue(35)
    );
  }

  public function testApplicationOf1(): void
  {
    $obj = new InventoryController();
    $obj->setPurchasesToProcess([self::purchaseObjectsArray()[0]]);
    $obj->setAppliedQty(self::$applied_qty);

    $this->assertEquals(
      4.2,
      $obj->getMoneyValue(1)
    );
  }
}
