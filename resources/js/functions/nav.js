export const navLinkClasses = (link, route) => {
  let classes = 'inline-block no-underline py-2 px-4'

  if (typeof route !== typeof undefined && link.path === route.path)
    classes += ' text-underline text-white'
  else
    classes += ' hover:text-gray-200 hover:text-underline text-gray-500'

  return classes
}
