export default [
  {
    label: 'Inventory',
    title: 'View inventory movements',
    path: '/inventory-movements'
  },
  {
    label: 'About',
    title: 'Go to About page',
    path: '/about'
  }
]
