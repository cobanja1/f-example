import HomeLinks from './HomeLinks'

export default [
  {
    label: 'Home',
    title: 'Go to the home page',
    path: '/'
  },
  ...HomeLinks
]
