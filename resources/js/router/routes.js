import MainLayout from './../layouts/MainLayout.vue'
import Home from './../components/pages/Home.vue'
import Inventory from './../components/pages/Inventory.vue'
import About from './../components/pages/About.vue'

export default [
  {
    path: '/',
    component: MainLayout,
    children: [
      { path: '', component: Home },
      { path: 'about', component: About },
      { path: 'inventory-movements', component: Inventory }
    ]
  }
]
