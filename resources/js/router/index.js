import { createRouter, createWebHistory } from 'vue-router'
import routes from './routes'

export default createRouter({
  mode: 'history',
  routes,
  history: createWebHistory()
})