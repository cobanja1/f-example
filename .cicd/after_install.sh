#!/bin/bash

SOURCE=f-example
UPLOADER=/usr/share/nginx/html/uploader/$SOURCE
TARGET=/usr/share/nginx/html/$SOURCE
COPY=copy-$SOURCE
BACKUP=backup
REPL_ARR=(app bootstrap config database public resources routes storage vendor)

if [ ! -w "$TARGET" ]; then
  echo "$TARGET is not writeable..."
  exit 1
fi

cd $UPLOADER

# Making a working copy to move
rm -r $COPY || true
cp -r $UPLOADER ../$COPY
mv ../$COPY $COPY

cd $COPY
# Adding required packages
composer update

# Running tests and failing if there is something wrong
mv .env.example .env
php artisan key:generate
./vendor/bin/phpunit

if [ $? -ne 0 ]; then
  echo "Unit Testing is failed"
  exit 1
fi

cd $UPLOADER

rm -rf $BACKUP || true
mkdir $BACKUP
mkdir $BACKUP/.replaced

cp -r $TARGET/* $BACKUP

# Preflight check for the folders
for R in "${REPL_ARR[@]}" ; do
  if [ -r "$TARGET/$R" ] && [ ! -w "$TARGET/$R" ]; then
    echo "$TARGET/$R cannot be moved to $BACKUP..."
    exit 1
  fi

  if [ ! -w "$COPY/$R" ]; then
    echo "$R cannot be deployed..."
    exit 1
  fi

  echo "$R can be deployed"
done

# Actual move of the production files
for R in "${REPL_ARR[@]}" ; do
  echo $R
  if [ -w "$TARGET/$R" ]; then
    mv $TARGET/$R $BACKUP/.replaced/$R
  fi

  mv $COPY/$R $TARGET/$R
done

rm -rf $COPY

find $TARGET -type d -exec chmod 755 {} \;
find $TARGET -type f -exec chmod 644 {} \;

PERM_ARR=(app bootstrap config database storage)

for P in "${PERM_ARR[@]}" ; do
  chown -R centos:nginx $TARGET/$P
  find $TARGET/$P -type d -exec chmod 775 {} \;
  find $TARGET/$P -type f -exec chmod 664 {} \;

  echo "Proper permissions are set for $P"
done
